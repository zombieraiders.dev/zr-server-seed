#!/bin/bash

set -x
set -e

# load the environment variables
export $(grep -v '^#' .env | xargs -0)

# set the base keys

DEPLOY=$(date +'%Y%m%dT%H%M%S')
ROOT=$(pwd)/server-data/dists
TARGET=$ROOT/${DEPLOY}

mkdir -p $TARGET 

# generate the maps and quests, sync the client data

cp -r Source/Client ${TARGET}
docker run --rm -v $(pwd)/Source:/source -v "$TARGET":/server-data zombieraiders/zr-api:$ZR_STACK_VERSION dna
cp -r Source/Channels ${TARGET}
cp server.properties server-data

./Scripts/MakeDists.bat

exit 0
