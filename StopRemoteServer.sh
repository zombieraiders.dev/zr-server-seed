#!/bin/bash

export $(grep -v '^#' .env | xargs -0)
export $(grep -v '^#' deploy-vars.env | xargs -0)

ssh -i $SSH_PRIVATE_KEY_FILE $ZR_SERVER_USER@$ZR_SERVER_ADDRESS docker-compose stop -t 300 
