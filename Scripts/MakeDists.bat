#!/bin/bash

export $(grep -v '^#' .env | xargs -0)

docker run --rm -v $(pwd)/server-data/dists:/server-data zombieraiders/zr-api:$ZR_STACK_VERSION dist
