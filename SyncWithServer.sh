#!/bin/bash

export $(grep -v '^#' .env | xargs -0)
export $(grep -v '^#' deploy-vars.env | xargs -0)

rsync -avz --delete -e "ssh -i $SSH_PRIVATE_KEY_FILE" server-data/ $ZR_SERVER_USER@$ZR_SERVER_ADDRESS:server-data/
scp -i $SSH_PRIVATE_KEY_FILE docker-compose.yaml .env $ZR_SERVER_USER@$ZR_SERVER_ADDRESS:.
